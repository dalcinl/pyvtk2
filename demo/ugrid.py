from pyvtk2 import *

points = [[0., 0., 0.],
          [0., 0., 1.],
          [0., 1., 0.],
          [0., 1., 1.],
          [1., 0., 0.],
          [1., 0., 1.],
          [1., 1., 0.],
          [1., 1., 1.]]

tetras = [[0, 2, 1, 5],
          [0, 2, 5, 4],
          [2, 3, 1, 5],
          [2, 3, 5, 7],
          [2, 4, 6, 5],
          [2, 6, 7, 5]]

vtk = VtkData(
    UnstructuredGrid(points, Tetra(tetras)),
    PointData(Scalars(range(8), 'pointdata')),
    CellData(Scalars(range(6), 'celldata')),
)

vtk.tofile('pyvtk2-ascii', format='ascii')
vtk.tofile('pyvtk2-binary', format='binary')
