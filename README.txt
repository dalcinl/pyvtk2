======
PyVTK2
======

:Author:       Lisandro Dalcin
:Contact:      dalcinl@gmail.com
:Organization: KAUST_

.. _KAUST:  https://www.kaust.edu.sa/

Tools for writing legacy VTK files in Python
