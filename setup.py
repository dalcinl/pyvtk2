# !/usr/bin/env python

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

metadata = {
    'keywords': ['VTK'],
    'classifiers': [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering :: Visualization',
    ],
    'platforms': 'All',
}

setup(
    name = 'PyVTK2',
    version = '0.2',
    description = 'PyVTK2 - tools for writing legacy VTK files in Python',
    url = 'https://bitbucket.org/dalcinl/pyvtk2',
    license = 'BSD-2-Clause',
    author = 'Lisandro Dalcin',
    author_email = 'dalcinl@gmail.com',
    packages = ['pyvtk2'],
    package_dir = {'': 'src'},
    provides = ['pyvtk2'],
    requires = ['numpy'],
    **metadata
)
