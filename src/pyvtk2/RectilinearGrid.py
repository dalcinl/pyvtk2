# --------------------------------------------------------------------

import numpy as _npy
from .DataSet import DataSet, _dump_array

# --------------------------------------------------------------------

class RectilinearGrid(DataSet):

    dataset_type = 'RECTILINEAR_GRID'

    data_type = 'double'
    npy_dtype = _npy.float64

    def __init__(self, x=(0,), y=(0,), z=(0,)):
        x = _npy.asarray(x, dtype=self.npy_dtype).reshape(-1)
        y = _npy.asarray(y, dtype=self.npy_dtype).reshape(-1)
        z = _npy.asarray(z, dtype=self.npy_dtype).reshape(-1)
        assert len(x) > 0
        assert len(y) > 0
        assert len(z) > 0
        self.x = x
        self.y = y
        self.z = z

    def dump_body(self, fd):
        nx, ny, nz = (len(self.x), len(self.y), len(self.z))
        fd.write('DIMENSIONS {:d} {:d} {:d}'.format(nx, ny, nz))
        fd.write('\n')
        for axis in ('X', 'Y', 'Z'):
            ax = getattr(self, axis.lower())
            fd.write(
                '{:s}_COORDINATES {:d} {:s}'.format(
                    axis, len(ax), self.data_type,
                )
            )
            fd.write('\n')
            _dump_array(ax, fd)
            fd.write('\n')

# --------------------------------------------------------------------

__all__ = ['RectilinearGrid']

# --------------------------------------------------------------------
