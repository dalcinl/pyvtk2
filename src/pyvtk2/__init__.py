"""
PyVTK2 - tools for writing legacy VTK files in Python.
"""

__author__   = 'Lisandro Dalcin <dalcinl@gmail.com>'
__credits__  = 'Pearu Peterson <pearu@cens.ioc.ee>'
__license__  = 'BSD-2-Clause'
__version__  = '0.2'

from .VtkData import *           # noqa: F403
from .DataSet import *           # noqa: F403
from .StructuredPoints import *  # noqa: F403
from .StructuredGrid import *    # noqa: F403
from .RectilinearGrid import *   # noqa: F403
from .UnstructuredGrid import *  # noqa: F403
