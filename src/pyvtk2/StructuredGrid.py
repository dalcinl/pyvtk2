# --------------------------------------------------------------------

import numpy as _npy
from .DataSet import DataSet
from .UnstructuredGrid import PointSet

# --------------------------------------------------------------------

class StructuredGrid(DataSet):

    dataset_type = 'STRUCTURED_GRID'

    def __init__(self, dimensions, points):
        dimensions = tuple(dimensions)
        points     = PointSet(points)
        assert len(points) == _npy.product(dimensions)
        self.dimensions = dimensions
        self.points     = points

    def dump_body(self, fd):
        fd.write('DIMENSIONS {:d} {:d} {:d}'.format(*self.dimensions))
        fd.write('\n')
        self.points.dump(fd)

# --------------------------------------------------------------------

__all__ = ['StructuredGrid']

# --------------------------------------------------------------------
