# --------------------------------------------------------------------

from .DataSet import DataSet

# --------------------------------------------------------------------

class StructuredPoints(DataSet):

    dataset_type = 'STRUCTURED_POINTS'

    def __init__(self, dimensions, origin=(0, 0, 0), spacing=(1, 1, 1)):
        dimensions = tuple(dimensions)
        origin     = tuple(origin)
        spacing    = tuple(spacing)
        assert len(dimensions) == 3
        assert len(origin) == 3
        assert len(spacing) == 3
        self.dimensions = dimensions
        self.origin     = origin
        self.spacing    = spacing

    def dump_body(self, fd):
        fd.write('DIMENSIONS {:d} {:d} {:d}'.format(*self.dimensions))
        fd.write('\n')
        fd.write('ORIGIN {:.15g} {:.15g} {:.15g}'.format(*self.origin))
        fd.write('\n')
        fd.write('SPACING {:.15g} {:.15g} {:.15g}'.format(*self.spacing))
        fd.write('\n')

# --------------------------------------------------------------------

__all__ = ['StructuredPoints']

# --------------------------------------------------------------------
