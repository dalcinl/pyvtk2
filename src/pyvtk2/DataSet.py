# --------------------------------------------------------------------

import numpy as _npy

def _file_is_binary(fd):
    return ('b' in fd.mode)

def _array_is_little_endian(array):
    _le = _npy.little_endian
    _bo = array.dtype.byteorder
    return (_le and (_bo == '=')) or (_bo == '<')

def _array_as_big_endian(array):
    if _array_is_little_endian(array):
        array = array.byteswap()
    return array

def _dump_array(array, fd):
    sep = ' '
    if _file_is_binary(fd):
        sep = ''
        array = _array_as_big_endian(array)
    array.tofile(fd, sep=sep)

# --------------------------------------------------------------------

class DataSet(object):

    dataset_type = None

    def dump(self, fd):
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        fd.write('DATASET {}'.format(self.dataset_type))
        fd.write('\n')

    def dump_body(self, fd):
        raise NotImplementedError

    def dump_tail(self, fd):
        pass

# --------------------------------------------------------------------

class DataSetAttr(object):

    attr_type = None
    attr_size = None

    data_type = 'double'
    npy_dtype = _npy.float64

    def __init__(self, data, name=None):
        data_array = _npy.asarray(data, dtype=self.npy_dtype)
        data_array = data_array.reshape(-1, self.attr_size)
        self.data_array = data_array
        if name is not None:
            assert isinstance(name, str)
            name = name.strip()
            assert len(name) > 0
            self.attr_name = name.replace(' ','_')
        else:
            name = str(self.attr_type).lower()
            self.attr_name = name

    def __len__(self):
        return len(self.data_array)

    def dump(self, fd):
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        header = '{} {} {}'.format(
            self.attr_type,
            self.attr_name,
            self.data_type,
        )
        fd.write(header)
        fd.write('\n')

    def dump_body(self, fd):
        _dump_array(self.data_array, fd)
        fd.write('\n')

    def dump_tail(self, fd):
        pass

class LookupTable(DataSetAttr):
    attr_type = 'LOOKUP_TABLE'
    attr_size = 4

    def __init__(self, data, name=None):
        DataSetAttr.__init__(self, data, name)
        assert self.data_array.min() >= 0
        assert self.data_array.max() <= 1

    def dump_head(self, fd):
        header = '{} {} {}'.format(
            self.attr_type,
            self.attr_name,
            len(self),
        )
        fd.write(header)
        fd.write('\n')

    def dump_body(self, fd):
        data = self.data_array
        if _file_is_binary(fd):
            data = data * 255
            data.round(out=data)
            data = data.astype('uint8')
        _dump_array(data, fd)
        fd.write('\n')

class Scalars(DataSetAttr):
    attr_type = 'SCALARS'
    attr_size = 1

    lookup_table = 'default'

    def __init__(self, data, name=None, lookup_table=None):
        DataSetAttr.__init__(self, data, name)
        if lookup_table is not None:
            assert isinstance(lookup_table, str)
            lookup_table = lookup_table.strip()
            assert len(lookup_table) > 0
            self.lookup_table = lookup_table.replace(' ','_')

    def dump_head(self, fd):
        DataSetAttr.dump_head(self, fd)
        fd.write('LOOKUP_TABLE {}'.format(self.lookup_table))
        fd.write('\n')

class Vectors(DataSetAttr):
    attr_type = 'VECTORS'
    attr_size = 3

class Normals(DataSetAttr):
    attr_type = 'NORMALS'
    attr_size = 3

class Tensors(DataSetAttr):
    attr_type = 'TENSORS'
    attr_size = 9

# --------------------------------------------------------------------

class DataSetAttrList(list):

    data_type = None

    def __init__(self, *sequence):
        list.__init__(self)
        for item in sequence:
            assert isinstance(item, DataSetAttr)
            assert len(item) == len(sequence[0])
        self.extend(sequence)

    def dump(self, fd):
        if not self:
            return
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        header = '{} {}'.format(self.data_type, len(self[0]))
        fd.write(header)
        fd.write('\n')

    def dump_body(self, fd):
        for item in self:
            item.dump(fd)

    def dump_tail(self, fd):
        pass

class PointData(DataSetAttrList):
    data_type = 'POINT_DATA'

class CellData(DataSetAttrList):
    data_type = 'CELL_DATA'

# --------------------------------------------------------------------

__all__ = (
    [cls.__name__ for cls in DataSetAttr.__subclasses__()] +
    [cls.__name__ for cls in DataSetAttrList.__subclasses__()]
)

# --------------------------------------------------------------------
