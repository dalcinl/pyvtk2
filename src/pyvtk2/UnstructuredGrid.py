# --------------------------------------------------------------------

import numpy as _npy
from .DataSet import DataSet, _dump_array

# --------------------------------------------------------------------

class UnstructuredGrid(DataSet):

    dataset_type = 'UNSTRUCTURED_GRID'

    def __init__(self, points, *cells):
        self.points = PointSet(points)
        self.cells  = CellSetList(cells)

    def dump_body(self, fd):
        self.points.dump(fd)
        self.cells.dump(fd)

# --------------------------------------------------------------------

class PointSet(object):

    data_type = 'double'
    npy_dtype = _npy.float64

    def __init__(self, data):
        point_array = _npy.asarray(data, dtype=self.npy_dtype)
        point_array = point_array.reshape(-1, 3)
        self.point_array = point_array

    def __len__(self):
        return len(self.point_array)

    def dump(self, fd):
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        fd.write('POINTS {:d} {:s}'.format(len(self), self.data_type))
        fd.write('\n')

    def dump_body(self, fd):
        _dump_array(self.point_array, fd)
        fd.write('\n')

    def dump_tail(self, fd):
        pass


class CellSetList(list):

    data_type = 'int'
    npy_dtype = _npy.int32

    def __init__(self, sequence):
        for item in sequence:
            assert isinstance(item, CellSet)
        self.extend(sequence)

    def dump(self, fd):
        if not self:
            return
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        pass

    def dump_body(self, fd):
        nc = sc = 0
        for item in self:
            nn = len(item)
            nc += nn
            sc += item.size + nn
        fd.write('CELLS {:d} {:d}'.format(nc, sc))
        fd.write('\n')
        for item in self:
            item.dump_data(fd)
        fd.write('CELL_TYPES {:d}'.format(nc))
        fd.write('\n')
        for item in self:
            item.dump_type(fd)

    def dump_tail(self, fd):
        pass

# --------------------------------------------------------------------

class CellSet(object):

    data_type = 'int'
    npy_dtype = _npy.int32

    cell_type = None
    cell_size = None

    def __init__(self, data):
        cell_array = _npy.asarray(data, dtype=self.npy_dtype)
        cell_array = cell_array.reshape(-1, self.cell_size)
        self.cell_array = cell_array

    def __len__(self):
        return len(self.cell_array)

    @property
    def size(self):
        return self.cell_array.size

    def dump_data(self, fd):
        data = self.cell_array
        m, n = data.shape
        tmp = _npy.empty((m, n+1), data.dtype)
        tmp[:, 0] = self.cell_size
        tmp[:, 1:] = data
        _dump_array(tmp, fd)
        fd.write('\n')

    def dump_type(self, fd):
        data = self.cell_array
        cell_type = _npy.asarray(self.cell_type, dtype='i')
        tmp = _npy.repeat(cell_type, data.shape[0])
        _dump_array(tmp, fd)
        fd.write('\n')

# ----------------------------------

class Vertex(CellSet):
    cell_type = 1
    cell_size = 1

class Line(CellSet):
    cell_type = 3
    cell_size = 2

class Triangle(CellSet):
    cell_type = 5
    cell_size = 3

class Quad(CellSet):
    cell_type = 9
    cell_size = 4

class Tetra(CellSet):
    cell_type = 10
    cell_size = 4

Tetrahedron = Tetra

class Hexahedron(CellSet):
    cell_type = 12
    cell_size = 8

Hexa = Hexahedron

class Wedge(CellSet):
    cell_type = 13
    cell_size = 6

class Pyramid(CellSet):
    cell_type = 14
    cell_size = 5

# ----------------------------------

class QuadraticEdge(CellSet):
    cell_type = 21
    cell_size = 3

class QuadraticTriangle(CellSet):
    cell_type = 22
    cell_size = 6

class QuadraticQuad(CellSet):
    cell_type = 23
    cell_size = 8

class QuadraticTetra(CellSet):
    cell_type = 24
    cell_size = 10

QuadraticTetrahedron = QuadraticTetra

class QuadraticHexahedron(CellSet):
    cell_type = 25
    cell_size = 20

QuadraticHexa = QuadraticHexahedron

# ----------------------------------

class PolyCellSet(CellSet):

    def __init__(self, cell_array):
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError

    @property
    def size(self):
        raise NotImplementedError

    def dump_data(self, fd):
        raise NotImplementedError

    def dump_type(self, fd):
        raise NotImplementedError

class PolyVertex(PolyCellSet):
    cell_type = 2

class PolyLine(PolyCellSet):
    cell_type = 4

class Polygon(PolyCellSet):
    cell_type = 7

# --------------------------------------------------------------------

__all__ = (
    ['UnstructuredGrid'] +
    [cls.__name__ for cls in CellSet.__subclasses__()] +
    [cls.__name__ for cls in PolyCellSet.__subclasses__()] +
    ['Tetrahedron', 'Hexa']
)

# --------------------------------------------------------------------
