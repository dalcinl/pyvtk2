# --------------------------------------------------------------------

import sys as _sys

from .DataSet import (
    DataSet,
    PointData,
    CellData,
)

# --------------------------------------------------------------------

class VtkData(object):

    version = (2, 0)
    title = 'VTK Data'

    def __init__(self, structure, point_data=None, cell_data=None):
        assert isinstance(structure, DataSet)
        if point_data is None:
            point_data = PointData()
        if cell_data is None:
            cell_data = CellData()
        assert isinstance(point_data, PointData)
        assert isinstance(cell_data,  CellData)
        self.structure  = structure
        self.point_data = point_data
        self.cell_data  = cell_data

    def tofile(self, filename, format='ascii'):
        assert isinstance(filename, str)
        assert format in ('ascii', 'binary')
        filename = filename.strip()
        if filename[-4:] != '.vtk':
            filename += '.vtk'
        if _sys.version_info.major >= 3:
            fd = open(filename, 'w', encoding='ascii')
            if format == 'binary':
                fd.mode = 'wb'
        else:
            mode = {'ascii': 'w', 'binary': 'wb'}
            fd = open(filename, mode[format])
        with fd:
            self.dump(fd)

    def dump(self, fd):
        self.dump_head(fd)
        self.dump_body(fd)
        self.dump_tail(fd)

    def dump_head(self, fd):
        assert len(self.version) == 2
        assert isinstance(self.title, str)
        assert len(self.title) <= 255
        header  = '# vtk DataFile Version {}.{}'
        fd.write(header.format(*self.version))
        fd.write('\n')
        fd.write(self.title[:255])
        fd.write('\n')
        if 'b' in fd.mode:
            format = 'binary'
        else:
            format = 'ascii'
        fd.write(format.upper())
        fd.write('\n')

    def dump_body(self, fd):
        self.structure.dump(fd)
        self.point_data.dump(fd)
        self.cell_data.dump(fd)

    def dump_tail(self, fd):
        pass

# --------------------------------------------------------------------

__all__ = ['VtkData']

# --------------------------------------------------------------------
