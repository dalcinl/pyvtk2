# -*- makefile -*-

.PHONY: build install sdist clean distclean

PYTHON = python

build:
	$(PYTHON) setup.py build $(opt)

install: build
	$(PYTHON) setup.py install --prefix='' --user $(opt)

sdist:
	$(PYTHON) setup.py sdist

clean:
	$(PYTHON) setup.py clean --all

distclean: clean 
	-$(RM) `find . -name '*~'`
	-$(RM) -r MANIFEST build dist src/*.egg-info
